import React from "react";
export default function ScrollTo() {
    const clickOne = () => document.getElementById('two')?.scrollIntoView({ behavior: 'smooth' });
    const clickTwo = () => document.getElementById('three')?.scrollIntoView({ behavior: 'smooth' });
    const clickThree = () => document.getElementById('one')?.scrollIntoView({ behavior: 'smooth' });
    return (
      <div>
        <div id="one" className="flex h-screen w-screen items-center justify-center bg-gray-200">
          <button
            type="button"
            className="font-inter text-7xl font-bold"
            onClick={() => clickOne()}
          >
            One
          </button>
        </div>
        <div id="two" className="flex h-screen w-screen  items-center justify-center bg-gray-600">
          <button
            type="button"
            className="font-inter text-7xl font-bold text-white"
            onClick={() => clickTwo()}
          >
            Two
          </button>
        </div>
        <div id="Three" className="flex h-screen w-screen  items-center justify-center bg-gray-900">
          <button
            type="button"
            className="font-inter text-7xl font-bold text-white"
            onClick={() => clickThree()}
          >
            Three
          </button>
        </div>
      </div>
    );
  }