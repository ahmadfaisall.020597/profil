import React from "react";
import Header from '../layouts/Header';
import About from '../about/AboutPage';
export default function ProfilPage() {
    return (
        <div className="w-screen h-screen bg-sky-800">
        <Header/>
        <About/>
        </div>
    );
}