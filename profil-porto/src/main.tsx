import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ProfilPage from './dashboard/profil/ProfilPage';
import { ChakraProvider } from '@chakra-ui/react'
import ScrollTo from './dashboard/ScrollTo';
import AboutPage from './dashboard/about/AboutPage';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ChakraProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/profil" element={<ProfilPage />} />
          <Route path="/scroll" element={<ScrollTo />} />
          <Route path="/about" element={<AboutPage />} />
        </Routes>
      </BrowserRouter>
    </ChakraProvider>
  </React.StrictMode>
)
